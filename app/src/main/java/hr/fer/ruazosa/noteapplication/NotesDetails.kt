package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_notes_details.*
import java.util.*

class NotesDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_details)

        val viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        var noteTitle = findViewById<EditText>(R.id.noteTitleEditText)
        var noteDesc = findViewById<EditText>(R.id.noteDescriptionEditText)

        if(NoteSinglenton.nodeIndex == -1){
            saveNoteButton.setText("Save Note")
        }else{
            saveNoteButton.setText("Update Note")
        }
        noteTitle.setText(NoteSinglenton.noteTitle)
        noteDesc.setText(NoteSinglenton.noteDescription)

        saveNoteButton.setOnClickListener {
            if(NoteSinglenton.nodeIndex == -1){
                var note = Note()
                note.noteTitle = noteTitleEditText.text.toString()
                note.noteDescription = noteDescriptionEditText.text.toString()
                note.noteDate = Date()
                viewModel.saveNoteToRepository(note)
            }else{
                var newNode = Note()
                newNode.noteTitle = noteTitle.text.toString()
                newNode.noteDescription = noteDesc.text.toString()
                newNode.noteDate =  Date()
                viewModel.updateNote(NoteSinglenton.nodeIndex, newNode)
            }
            finish()
        }
    }
}
