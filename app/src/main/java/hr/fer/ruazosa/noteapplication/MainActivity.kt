package hr.fer.ruazosa.noteapplication


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.DialogInterface
import android.content.Intent
import android.nfc.Tag
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.note_list_element.*


class MainActivity : AppCompatActivity(),NotesAdapter.OnNoteListener  {

    lateinit var notesAdapter: NotesAdapter
    lateinit var viewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listOfNotesView.layoutManager = LinearLayoutManager(applicationContext)

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.cell_divider)!!)
        listOfNotesView.addItemDecoration(decorator)

        viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        notesAdapter = NotesAdapter(viewModel, this)
        listOfNotesView.adapter = notesAdapter

        viewModel.notesList.observe(this, Observer {
            notesAdapter.notifyDataSetChanged()
        })

        newNoteActionButton.setOnClickListener {
            val intent = Intent(this, NotesDetails::class.java)
            NoteSinglenton.nodeIndex = -1
            NoteSinglenton.noteTitle = ""
            NoteSinglenton.noteDescription = ""
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.getNotesRepository()
    }

    override fun onNoteClick(postion: Int) {
        Log.d("TAG", "POSITION:  " + postion)
        val intent = Intent(this, NotesDetails::class.java)
        var note = viewModel.getNoteFromRepository(postion)

        NoteSinglenton.noteTitle = note?.noteTitle
        NoteSinglenton.noteDescription = note?.noteDescription
        NoteSinglenton.noteDate = note?.noteDate
        NoteSinglenton.nodeIndex = postion
        startActivityForResult(intent, 0)
    }

    override fun onDeleteClick(postion: Int) {
        val builder = AlertDialog.Builder(this@MainActivity)

        builder.setTitle("Delete Alert")
        builder.setMessage("Do you want to delete note ${viewModel.getNoteFromRepository(postion).noteTitle}  ?")

        builder.setPositiveButton("YES"){dialog, which ->
            viewModel.deleteNoteFromRepository(postion)
            notesAdapter.notifyDataSetChanged()
        }

        builder.setNeutralButton("Cancel"){_,_ ->

        }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }


}
