package hr.fer.ruazosa.noteapplication
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class NotesAdapter(listOfNotesViewModel: NotesViewModel,  onNoteListener: OnNoteListener): RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    var listOfNotes: NotesViewModel = listOfNotesViewModel
    var mOnNoteListener: OnNoteListener


    init {
        this.mOnNoteListener = onNoteListener
    }

    class ViewHolder(itemView: View, onNodeListener: OnNoteListener): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var noteTitleTextView: TextView? = null
        var noteDateTextView: TextView? = null
        var onNodeListener: OnNoteListener? = null
        var deleteImage: ImageView

        init {
            noteTitleTextView = itemView.findViewById(R.id.noteTitleTextView)
            noteDateTextView = itemView.findViewById(R.id.noteDateTextView)
            this.onNodeListener = onNodeListener
            itemView.setOnClickListener(this)

            this.deleteImage = itemView.findViewById(R.id.image_delete)

            this.deleteImage.setOnClickListener {
                onNodeListener?.onDeleteClick(adapterPosition)
            }
        }

        override fun onClick(p0: View?) {
            onNodeListener?.onNoteClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NotesAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notesListElement = inflater.inflate(R.layout.note_list_element, parent, false)
        return ViewHolder(notesListElement,this.mOnNoteListener)
    }

    override fun getItemCount(): Int {
        if (listOfNotes.notesList.value != null) {
            return listOfNotes.notesList.value!!.count()
        }
        return 0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (listOfNotes != null) {
            viewHolder.noteTitleTextView?.text = listOfNotes.notesList.value!![position].noteTitle


            viewHolder.noteDateTextView?.text =
                listOfNotes.notesList.value!![position].noteDate.toString()

        }
    }

    interface OnNoteListener{
        fun onNoteClick(postion : Int)
        fun onDeleteClick(postion: Int)
    }
}