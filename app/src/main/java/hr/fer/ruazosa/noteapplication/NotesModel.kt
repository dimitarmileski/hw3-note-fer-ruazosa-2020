package hr.fer.ruazosa.noteapplication

import java.util.Date

class Note{
    var noteTitle: String ? = null
    var noteDescription: String ? = null
    var noteDate: Date? = null

   constructor(){

   }
    constructor(noteTitle: String? , noteDescription: String ?, noteDate: Date?){
        this.noteTitle = noteTitle
        this.noteDescription = noteDescription
        this.noteDate = noteDate
    }


}

