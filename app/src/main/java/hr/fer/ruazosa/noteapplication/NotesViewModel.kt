package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class NotesViewModel: ViewModel() {
    var notesList = MutableLiveData<List<Note>>()

    fun getNotesRepository() {
        notesList.value = NotesRepository.notesList
    }

    fun saveNoteToRepository(note: Note) {
        NotesRepository.notesList.add(note)
    }

    fun getNoteFromRepository(position: Int): Note{
        return notesList.value!!.get(position)
    }

    fun updateNote(position: Int, note: Note){
        NotesRepository.notesList.set(position, note)
    }
    fun deleteNoteFromRepository(noteIndex: Int) {
        NotesRepository.notesList.removeAt(noteIndex)
    }
}